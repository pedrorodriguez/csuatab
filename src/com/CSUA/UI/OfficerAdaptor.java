package com.CSUA.UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.CSUA.backend.Officer;
import com.example.CSUATab.R;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: keiga
 * Date: 3/16/13
 * Time: 1:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class OfficerAdaptor extends ArrayAdapter<Officer> {
    private List<Officer> officerList;
    private Context context;
    public OfficerAdaptor(List<Officer> officerList, Context ctx) {
        super(ctx, R.layout.officer_layout, officerList);
        this.officerList = officerList;
        this.context = ctx;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.officer_layout, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.name)).setText(officerList.get(position).getName());
        ((TextView) convertView.findViewById(R.id.tab)).setText(officerList.get(position).getTab() + "");
        ((TextView) convertView.findViewById(R.id.tabid)).setText(officerList.get(position).getTabId());
        ((TextView) convertView.findViewById(R.id.sid)).setText(officerList.get(position).getSid());
        return convertView;
    }
}
