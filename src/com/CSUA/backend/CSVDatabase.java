package com.CSUA.backend;

import java.io.*;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: keiga
 * Date: 3/16/13
 * Time: 12:29 AM
 * To change this template use File | Settings | File Templates.
 */
public class CSVDatabase {
    public File dbFile;
    public HashMap<Integer, Officer> officers;
    private int max;
    public CSVDatabase(File dbFile, boolean load) {
        this.dbFile = dbFile;
        officers = new HashMap<Integer, Officer>();
        if (load) {
            loadDatabase();
        }
    }
    public void loadDatabase() {
        flushDatabase();
        File officerFile = dbFile;
        FileInputStream fis = null;
        InputStreamReader iReader = null;
        BufferedReader br = null;
        try {
            fis = new FileInputStream(officerFile);
            iReader = new InputStreamReader(fis);
            br = new BufferedReader(iReader);
            String line = br.readLine();
            max = Integer.parseInt(line);
            while ((line = br.readLine()) != null) {
                Officer officer = new Officer(line);
                if (officer.getTabId() == -1) {
                    officer.setTabId(max);
                    max += 1;
                }
                officers.put(officer.getTabId(), officer);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException();
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
    public void saveDatabase() {
        File officerFile = dbFile;
        try {
            PrintWriter bw = new PrintWriter(new FileWriter(officerFile, false));
            bw.println(Integer.toString(max));
            for(Officer o : officers.values()) {
                bw.println(o.toString());
            }
            bw.flush();
            bw.close();
        }
        catch(IOException e){
            throw new RuntimeException();
        }
    }
    public void flushDatabase() {
        officers.clear();
    }
}
